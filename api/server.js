const fs = require('fs');
const bodyParser = require('body-parser');
const jsonServer = require('json-server');
const jwt = require('jsonwebtoken');
const getData = require('./index');

const server = jsonServer.create();
const router = jsonServer.router(getData());
const userdb = JSON.parse(fs.readFileSync('./users.json', 'utf-8'));


server.use(jsonServer.defaults());
server.use(jsonServer.bodyParser);


const SECRET_KEY = '123456789';

const expiresIn = '1h';

// Create a token from a payload
function createToken(payload) {
  return jwt.sign(payload, SECRET_KEY, { expiresIn });
}

// Verify the token
function verifyToken(token) {
  return jwt.verify(token, SECRET_KEY, (err, decode) => (decode !== undefined ?  decode : err));
}

// Check if the user exists in database
function isAuthenticated({ username, password }) {
  return userdb.users.find(user => user.username === username && user.password === password);
}


server.post('/auth/login', (req, res) => {
  const { username, password } = req.body;
  const isValidUser = isAuthenticated({ username, password });
  if (!isValidUser) {
    const status = 401;
    const message = 'Incorrect username or password';
    res.status(status).json({ status, message });
    return;
  }
  const ACCESSTOKEN = createToken({ username, password });
  res.status(200).json({ access_token: ACCESSTOKEN, user: isValidUser.name });
});

server.use(/^(?!\/auth).*$/, (req, res, next) => {
  if (req.headers.authorization === undefined || req.headers.authorization.split(' ')[0] !== 'Bearer') {
    const status = 401;
    const message = 'Error in authorization format';
    res.status(status).json({ status, message });
    return;
  }
  try {
    verifyToken(req.headers.authorization.split(' ')[1]);
    next();
  } catch (err) {
    const status = 401;
    const message = 'Error access_token is revoked';
    res.status(status).json({ status, message });
  }
});

server.use(jsonServer.rewriter({
  '/movies/:id/turns/:turnId': '/turns/:turnId',
  '/movies/:id/turns': '/turns'
}))

server.use(router);

server.listen(3006, () => {
  console.log('Run Auth API Server');
});
