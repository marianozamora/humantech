const faker = require('faker');
const _ = require('lodash');

let random = (min, max) => Math.floor(Math.random() * (max - min + 1) + min);

let minMinute = 10;
let maxMinute = 59;

let minHour = 10;
let maxHour = 23;


let generateTimeFake = () => {
  let randomMinute = random(minMinute, maxMinute);
  let randomHour = random(minHour, maxHour);
  return `${randomHour}:${randomMinute}`
}

module.exports = () => {
  let data = {
    movies: _.times(10, n => ({
        id: n,
        name: faker.name.findName(),
        date: faker.date.past(),
        isActive: true
      })),
    turns: _.times(8, n => ({
      id: n,
      turn: generateTimeFake(),
      isActive: false,
    }))
  }
  return data;
};
