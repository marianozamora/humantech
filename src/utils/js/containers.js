/**
 * Function to generate multiple containers
 * @param {function} callback function to render
 * @param {string} action string with the action
 * @param {object} props manage routing props
 */
const generateContainer = (callback, action, props) => callback({ actionToPage: action }, props);
/**
 * end
 */

export default generateContainer;
