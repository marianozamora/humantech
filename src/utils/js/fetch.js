import axios from 'axios';


const getToken = () => {
  const token = localStorage.token_web;
  return token ? `Bearer ${token}` : false;
};

const getHeaders = () => {
  let headers;
  const token = getToken();
  if (token) {
    headers = { 'Accept-Language': 'es', Authorization: token };
  }
  return headers;
};

export const generateFetch = (config) => {
  const newConfig = {
    ...config,
    headers: getHeaders(),
  };
  return axios(newConfig);
};


export const serializeUrl = (source) => {
  const array = [];
  Object.keys(source).forEach(key => array.push(`${encodeURIComponent(key)}=${encodeURIComponent(source[key])}`));
  return array.join('&');
};
