export const BASEHOST = process.env.REACT_APP_BASE_DOMAIN;
export const STATIC_URL = `${BASEHOST}/media/`;
export const CLIENTHOST = `${BASEHOST}api/client/`;