import React from 'react';
import moment from 'moment';
import styled from 'styled-components';
import {Link} from 'react-router-dom';

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPen, faTrash, faBars } from "@fortawesome/free-solid-svg-icons";


const Wrapper = styled.div `
  background-color: #ffd000 !important;
  display: block;
  line-height: 40px;
  color: #00964B;
  border-radius: 20px;
  padding: 0px 10px;
`;

export const statusFormat = (status) => {
  return <Wrapper>{status ? 'Activo' : 'Inactivo'}</Wrapper>;
};

export const dateFormat = (value, date) => {
  return (!date) ? moment(value).format('DD/MM/YYYY') : moment(date).format('DD/MM/YYYY');
};

export const actionFormat = (id, fnCallbackDelete, fnCallbackEdit, fnCallbackAsign, movieId) => {
  return (
    <div style={{display:'flex'}}>
      <button onClick={() => fnCallbackEdit(id)} className="btn-white btn btn-xs" type="button">
        <FontAwesomeIcon icon={faPen} />
      </button>
      <Link to={`movie/${id}/turns`}>
        <FontAwesomeIcon icon={faBars} />
      </Link>
      <button onClick={() => fnCallbackDelete(id)} className="btn-white btn btn-xs" type="button">
        <FontAwesomeIcon icon={faTrash} />
      </button>
    </div>
  )
}

export const actionFormatTurn = (id, fnCallbackDelete, fnCallbackEdit, movieId) => {
  return (
    <div style={{display:'flex'}}>
      <button onClick={() => fnCallbackEdit(movieId, id)} className="btn-white btn btn-xs" type="button">
        <FontAwesomeIcon icon={faPen} />
      </button>
      <button onClick={() => fnCallbackDelete(movieId, id)} className="btn-white btn btn-xs" type="button">
        <FontAwesomeIcon icon={faTrash} />
      </button>
    </div>
  )
}