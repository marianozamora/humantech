import * as moment from 'moment';

export const onChange = (e, context) => {
  context.setState({ [e.target.name]: e.target.value });
};

export const onSubmit = (e, context, callback, params) => {
  e.preventDefault();
  context.props[callback](params);
};

export const onClick = (e, callback) => {
  e.preventDefault();
  callback();
};

export const manageSelect = (value, elements, isMultiple) => {
  let selectStatus;
  if (!isMultiple) {
    selectStatus = elements.reduce((acc, val, index) => {
      if (value === val.value) {
        acc = value;
      }
      return acc;
    }, '');
  } else {
    selectStatus = elements.reduce((acc, val, index) => {
      if (value == val.value) {
        if (val.selected) {
          elements[index].selected = false;
        } else {
          elements[index].selected = true;
        }
      }
      if (elements[index].selected) {
        acc.push(val.value);
      }
      return acc;
    }, []);
  }
  return selectStatus;
};


export const parseStateToValue = state => (state ? moment(state) : '');
export const formatValueToState = value => value.toISOString();
