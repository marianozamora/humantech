import axios from 'axios';

const getToken = () => {
  const token = localStorage.token_admin;
  return token ? `JWT ${token}` : false;
};

const getHeaders = () => {
  let headers;
  const token = getToken();
  if (token) {
    headers = { 'Accept-Language': 'es', Authorization: token };
  }
  return headers;
};

const generateFetch = (config) => {
  const newConfig = {
    ...config,
    headers: getHeaders(),
  };
  return axios(newConfig);
};


export default generateFetch;
