import styled from 'styled-components';
import { media } from '../css/media';

const Flexrow = styled.div`
  display: flex;
  ${media.tablet_cutoff`
    flex-wrap: wrap;
  `}
`;

const Flexcolumn = styled.div`
  text-align: center;
  width: ${props => (props.size / 12) * 100}%;
  ${media.tablet_cutoff`
    width: ${props => (props.size / ((props.size > 6)) ? 12 : 6) * 100}%;
  `}
`;

const FlexcolumnLeft = styled.div`
  text-align: center;
  width: ${props => (props.size / 12) * 100}%;
  ${media.tablet_cutoff`
    width: ${props => (props.size / ((props.size > 6)) ? 12 : 6) * 100}%;
  `}
`;

const FlexcolumnThree = styled(Flexcolumn)`
  ${media.tablet_cutoff`
    width: ${props => (props.size / 4) * 100}%;
  `}
`;

export { Flexrow, Flexcolumn, FlexcolumnThree, FlexcolumnLeft };
