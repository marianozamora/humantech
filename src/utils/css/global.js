import { injectGlobal } from 'styled-components';
import { fonts, colors } from './variables';


const GlobalStyles = injectGlobal`
  *, *:before, *:after {
    box-sizing: border-box;
    outline:none;
    //outline: 1px solid tomato;
  }
  html,body {
    font-family: Brevia, sans-serif, arial;
    text-align:center;
    display: inline-block;
    font-size: ${fonts.font_base};
    width:100%;
    color: #5b5b5f;
  }
  ul {
    list-style: none;
    margin: 0;
    padding: 0;
  }
  a {
    text-decoration:none;
    color:${colors.white};
  }
  input {
    border:none;
    font-size:16px !important
    background:transparent;
  }
  input[type="text"],
  input[type="email"]{
    box-sizing: border-box;
  }
  select {
    background: ${colors.white};
    border-radius: 3px;
    border: none;
    padding:8px 26px;
    appearance:none;
  }
  option{
    color:black;
  }
  input[type='text']{
    &:focus{ outline:none;}
  }`;

export default GlobalStyles;
