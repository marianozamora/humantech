/**
 * Variables for style components
 */
export const colors = {
  // primary colors
  white: '#ffffff',
  green: '#34AC5D',
  dark_green: '#007748',
  dark_gray: '#4a4a4a',
  gray: '#6a6a6a',
  green_search_title: '#3c964a',
  logo_green: '#00964B',
  yellow: '#f4cf00',
  red: '#f7323f',
  // secondary colors
  sky_blue: '#5FA2F0',
  // gray
  middle_gray: '#9A9FA6',
  medium_gray: '#5b5b5f',
  light_gray: '#E3E4E7',
  hover_gray: '#f5f5f5',
  border_gray: '#cccccc',
  // error
  error_color: '#D55C4B',
  // hover
  dark_green_2: '#419d3e',
  dark_sky_blue: '#4483CC',
  // danger
  danger: '#FF5E70',
  dark_danger: '#CF4B5A',
  // background
  lima_gray: '#F5F6F7',
  // purse
  purse_gray: '#3D5469',
};

export const breakpoints = {
  mobile: '320px',
  mobile_cutoff: '480px',
  tablet: '780px',
  tablet_cutoff: '840px',
  desktop: '1140px',
};

export const fonts = {
  font_color: 'black',
  base_font: '16px',
  font_family: 'arial, sans-serif',
};

export const extra = {
  container_width: '1140px',
  border_radius: '10px',
  input_height: '2.5rem',
  transition: '0.3s all ease-in',
  box_shadow: '0 15px 20px rgba(0, 0, 0, 0.3)',
  shadow: '1px 2px 4px rgba(0, 0, 0, .5)',
  inputPadding: '10px',
};
