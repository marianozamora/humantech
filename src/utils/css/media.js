import { css } from 'styled-components';
import { breakpoints } from './variables';

const media = {
  mobile: (...args) => css`@media (max-width: ${breakpoints.mobile}) { ${css(...args)} }`,
  mobile_cutoff: (...args) => css`@media (max-width: ${breakpoints.mobile_cutoff}) { ${css(...args)} }`,
  tablet: (...args) => css`@media (max-width: ${breakpoints.tablet}) { ${css(...args)} }`,
  tablet_cutoff: (...args) => css`@media (max-width: ${breakpoints.tablet_cutoff}) { ${css(...args)} }`,
  desktop: (...args) => css`@media (max-width: ${breakpoints.desktop}) { ${css(...args)} }`,
};

export default media;
