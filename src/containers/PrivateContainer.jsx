import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { Switch, Route, Redirect } from 'react-router';
import { withRouter } from 'react-router-dom';
import generateContainer from '../utils/js/containers';
import HomeContainer from './HomeContainer';
import Header from '../components/04-layouts/Header';
import { logoutUser, getUser } from '../actions/userActions';
import BackgroundPrivate from '../components/04-layouts/BackgroundPrivate';
import MenuApp from '../components/03-components/Nav/MenuApp';
import NavApp from '../components/03-components/NavApp/NavApp';
import styled from 'styled-components';

const Aside = styled.div`
  text-align: left;
  margin-right: 1em;
  border-right: 1px solid gray;
  padding-right: 1em;
  & li {
    margin: 1em;
  }
`;
class PrivateContainer extends React.Component {
  componentWillReceiveProps(nextProps) {
    if ((this.props.isAuthenticated !== nextProps.isAuthenticated)  && !nextProps.isAuthenticated) this.props.history.push('/login/');
  }

  render() {
    return (
      <div>
        <Header isPrivate logoClass="e-logo-header" className="u-container-header" title="HUMANTECH">
          <MenuApp fnLogout={this.props.logoutUser} />
          <NavApp />
        </Header>
        <BackgroundPrivate>
        <Aside>
          <ul>
            <li>Dashboard</li>
            <li>Peliculas</li>
            <li>Turnos</li>
            <li>Administradores</li>
            <li>Perfiles</li>
            <li>Cerrar Sesion</li>
          </ul> 
        </Aside>
          <Switch>
            <Route
              exact
              path="/movies"
              component={props =>
                generateContainer(HomeContainer, 'movie', props)}
            />
            <Route
              exact
              path="/movie/:id/turns"
              component={props =>
                generateContainer(HomeContainer, 'turns', props)}
            />
            <Redirect from="/" to="/movies" />
          </Switch>
        </BackgroundPrivate>
      </div>
    );
  }
}

PrivateContainer.propTypes = {
  logoutUser: PropTypes.func.isRequired,
  history: PropTypes.oneOfType([
    PropTypes.object,
  ]).isRequired,
  location: PropTypes.oneOfType([
    PropTypes.object,
  ]).isRequired,
};

const mapStateToProps = state => (state.account.login);

const mapDispatchToProps = dispatch => (
  bindActionCreators({
    logoutUser,
    getUser,
  }, dispatch)
);
PrivateContainer.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired,
};
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(PrivateContainer));
