import React from 'react';
import MoviePage from '../components/05-pages/Movies/MoviesPage';
import TurnsPage from '../components/05-pages/Movies/TurnsPage';

import Wrapper from '../components/04-layouts/Wrapper';

const HomeContainer = ({ actionToPage }, props) => {
  const chooseAction = () => {
    switch (actionToPage) {
      case 'movie':
        return <MoviePage routeProps={props} />;
      case 'turns':
        return <TurnsPage routeProps={props}></TurnsPage>;
      default:
        return <div> DEFAULT PAGE </div>;
    }
  };
  return (
    <Wrapper className="u-textLeft">
      {chooseAction()}
    </Wrapper>
  );
};

export default HomeContainer;
