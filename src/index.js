import 'normalize.css';
import React from 'react';
import ReactDOM from 'react-dom';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from 'react-router-dom';
import { Provider } from 'react-redux';
import store from './store';
import PrivateContainer from './containers/PrivateContainer';
import LoginPage from './components/05-pages/Account/Login/LoginPage';
import './utils/css/global';
import './components/00-helpers/css/utils.css';
import './components/00-helpers/css/custom.css';
import './components/00-helpers/css/fonts.css';




function ProtectedRouter() {
  const isAuthenticated = !!localStorage.getItem('token_web');
  return isAuthenticated ? <Route path="/" component={PrivateContainer} /> : <Redirect to="/login" />;
}


ReactDOM.render(
  <Provider store={store}>
    <Router>
      <Switch>
        <Route path="/login" component={LoginPage} />
        <ProtectedRouter /> 
      </Switch>
    </Router>
  </Provider>,
  document.getElementById('root'),
);


// registerServiceWorker()
