import styled from 'styled-components';
import { NavLink } from 'react-router-dom';
import { colors } from '../../../utils/css/variables';
import media from '../../../utils/css/media';

const ListLink = styled(NavLink)`
  border: 2px solid ${colors.sky_blue};
  color: ${colors.sky_blue};
  border-radius: 5px;
  font-weight: bold;
  padding: 5px 15px;
  margin: 1em;
  ${media.tablet`
    display: block;
    margin: 0;
  `}
`;

export default ListLink;
