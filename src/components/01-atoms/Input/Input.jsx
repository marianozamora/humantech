import React from 'react';
import { InputText, LabelInput } from './Input.styles';

const InputMaterialize = params => (
  <div style={{ position: 'relative', margin: '0.5em auto', width: '100%' }}>
    {params.label && (
      <LabelInput>
        {params.label}
      </LabelInput>
    )}
    <InputText {...params} />
  </div>
);

export default InputMaterialize;
