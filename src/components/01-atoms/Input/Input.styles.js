import styled from 'styled-components';
import { colors, extra } from '../../../utils/css/variables';


export const LabelInput = styled.label`
  font-size:1em;
  color: ${colors.medium_gray};
  font-family: inherit;
  margin: 1.2em 0;
  font-weight: bold;
  font-size: 1.1em;
  pointer-events:none;
  display:flex;
  transition:0.2s ease all;
`;
export const InputText = styled.input`
  font-size:18px;
  padding: ${extra.inputPadding};
  display:block;
  width:100%;
  min-height:50px;
  background-color:transparent;
  border:1px solid ${colors.green};
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0px -15px 10px -12px rgba(0, 0, 0, 0.05);
  border-radius: ${extra.border_radius}
`;

export const inputStyles = {
  fontSize: '18px',
  padding: '10px 10px 10px 5px',
  display: 'block',
  width: '100%',
  background: 'transparent',
  border: 'none',
  borderBottom: `1px solid ${colors.medium_gray}`,
/*   '&:focus': {
    outline:'none',
    border:'none',
  } */
};
