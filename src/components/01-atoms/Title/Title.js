import styled from 'styled-components';

const H1 = styled.h1`
  font-size: 2em;
`;

const H2 = H1.withComponent('h2').extend`
  font-size: 1.7em;
`;

const H3 = H1.withComponent('h3').extend`
  font-size: 1.5em;
`;

const H4 = H1.withComponent('h4').extend`
  font-size: 1em;
`;

const H5 = H1.withComponent('h5').extend`
  font-size: 1em;
`;

const H6 = H1.withComponent('h6').extend`
  font-size: 0.5em;
`;

export { H1, H2, H3, H4, H5, H6 };
