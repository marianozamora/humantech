import styled from 'styled-components';
import { colors, extra } from '../../../utils/css/variables';

const Button = styled.button`
background: ${props => (colors[props.color] ? colors[props.color] : 'white')};
color: ${props => (colors[props.color] ? 'white' : colors.medium_gray)};
outline:none;
cursor:pointer;
font-size: 1em;
margin: 1em auto;
display: block;
width:100%;
padding:  ${props => (props.padding ? props.padding : '0.6em 2.5em')};
border: 2px solid ${props => colors[props.color]};
border-radius: ${extra.border_radius};
&[disabled]{
  opacity:0.5;
  cursor: not-allowed;
  border:2px solid transparent;
}
`;

export default Button;
