import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { colors } from '../../../utils/css/variables';

const Link = ({
  className, children, isDownload, url, onClick,
}) => (
  <a className={className} href={url} onClick={(e) => {e.preventDefault(); onClick();} }>
    { children }
  </a>
);

const activeClassName = 'nav-item-active';

export const NavItem = styled(NavLink).attrs({
  activeClassName,
})`
color: #00964B;
margin: 0 1.5em;
cursor: pointer;
font-size: 1.2em;
text-decoration: none;
  &.${activeClassName} {
  padding-bottom:0.5em;
  border-bottom: 2px solid ${colors.green};
}
`;

Link.defaultProps = {
  className: '',
  isDownload: '',
  url: '',
};

Link.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node.isRequired,
  isDownload: PropTypes.string,
  url: PropTypes.string,
};

const StyledLink = styled(Link)`
    font-size: 1em;
    color: #00964B;
    font-weight: bold;
    text-decoration: none;
    padding-bottom: 5px;
    border-bottom: 1px solid #00964B;
    font-family:Arial;
`;


export default StyledLink;
