import styled from 'styled-components';
import { NavLink } from 'react-router-dom';
import media from '../../../utils/css/media';
import { colors } from '../../../utils/css/variables';

const HomeLink = styled(NavLink)`
  background-color: ${colors.green};
  border-radius: 5px;
  padding: 0.5em 2em;
  margin: 1em;
  &:hover {
    border-bottom: 2px solid ${colors.dark_green};
  }
  ${media.tablet`
    display: block;
    margin: 1em 2em;
    padding: 1em;
  `}
  ${media.mobile`
    margin: 1em 0em;
  `}
`;

export default HomeLink;
