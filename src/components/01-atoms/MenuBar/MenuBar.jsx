import React, { Component } from 'react';
import styled from 'styled-components';
import media from '../../../utils/css/media';
import '../../00-helpers/css/custom.css';

const WrapMenu = styled.div`
  display: none;
  position: relative;
  min-height: 30px;
  min-width: 50px;
  ${media.tablet_cutoff`
    display: block;
    z-index: 2;
  `}
`;

const Inner = styled.div`
  &, &:before, &:after {
    left: 0;
    background-color: #FFF;
    position: absolute;
    width: 40px;
    height: 4px;
    border-radius: 5px;
    content: '';
    transition-duration: .3s;
    transition-timing-function: ease;
    transition-property: transform, opacity;
  }
  &:before {
    top: 10px;
  }
  &:after {
    top: 20px;
  }
`;

class MenuBar extends Component {
  constructor(props) {
    super(props);
    this.state = { active: false };
    this.handleClick = this.handleClick.bind(this);
  }

  componentDidMount() {
    document.body.style.overflow = 'visible';
  }

  handleClick() {
    const menu = document.querySelector('.menu-bar');

    if (menu.style.display === 'block') {
      document.body.style.overflow = 'visible';
      menu.style.display = 'none';
    } else {
      document.body.style.overflow = 'hidden';
      menu.style.display = 'block';
    }

    if (!this.state.active) this.setState({ active: true });
    else this.setState({ active: false });
  }

  render() {
    const active = this.state.active ? 'active' : '';
    const classes = `${active} menu`;

    return (
      <WrapMenu className={classes} onClick={this.handleClick}>
        <Inner className="menu-inner" />
      </WrapMenu>
    );
  }
}

export default MenuBar;
