import React from 'react';
import PropTypes from 'prop-types';
// Get all icons
const files = require.context('!svg-sprite-loader!../../../assets/icons', false, /.*\.svg$/);
files.keys().forEach(files);

const Icon = ({
  type, className, size, fill,
}) => (
  <svg
    className={`${className}`}
    fill={fill}
    height={size}
    width={size}
  >
    <use xlinkHref={`#${type}`} />
  </svg>
);

Icon.defaultProps = {
  className: '',
};

Icon.propTypes = {
  type: PropTypes.string.isRequired,
  size: PropTypes.string.isRequired,
  fill: PropTypes.string.isRequired,
  className: PropTypes.string,
};

export default Icon;
