import React from 'react';
import PropTypes from 'prop-types';
import ErrorMessage from './ErrorMessage';
import Input from '../01-atoms/Input/Input';
import SelectForm from './SelectForm';
import DatePicker from 'react-datepicker';
import * as moment from 'moment';
import {
  LabelInput
} from '../01-atoms/Input/Input.styles';

import 'react-datepicker/dist/react-datepicker.css';

const getWidth = (size) => {
  const width = (size / 12) * 100;
  return `${width}%`;
};

const chooseTypeInput = (props) => {
  const {
    type, input, options, placeholderDinamic, selectValue, withLabel, firstOptionLabel,
    customClass, customStyle, label
  } = props;
  let value;
  switch (type) {
    case 'date': 
      return (
        <div style={{ position: 'relative', margin: '0.5em auto', width: '100%' }}>
          <LabelInput>
            {label}
          </LabelInput>
          <DatePicker
            selected = {
              new Date(props.input.value ? props.input.value : Date.now())
            }
            {...props}
            dateFormat="MM-dd-yyyy"
            name={props.input.name}
            value={props.input.value}
            onChange={props.input.onChange}
          />
        </div>
      );
    case 'select':
      return (
        <div style={{ position: 'relative', margin: '0.5em auto', width: '100%' }}>
            <LabelInput>
              {label}
            </LabelInput>
           <SelectForm
            withLabel={withLabel}
            options={options}
            customClass={customClass}
            customStyle={customStyle}
            customName={input.name}
            firstOptionLabel={firstOptionLabel}
          />
        </div>
      );
    case 'text':
    case 'password':
    case 'time':
      return (
        <Input
          {...props}
          name={props.input.name}
          value={props.input.value}
          onChange={props.input.onChange}
          placeholder={(value && value.name) ? `Ingresar el ${value.name}` : ''}
        />);
    default:
      break;
  }
};

const InputForm = (props) => {
  const {
    meta, error, size, block, padding, minWidth, minHeight, className, input,
  } = props;
  const messageError = meta ? meta.error : error;
  let ErrorProps = {};
  if (messageError) {
    ErrorProps = {
      error: messageError,
      touched: meta ? meta.touched : true,
    };
  }
  const width = size ? getWidth(size) : 'initial';
  const display = block || 'inline-block';
  return (
    <div
      className={`u-element-wrap ${className}`}
      style={{
        display: `${display}`,
        width: `${width}`,
        padding: `${padding}`,
        minHeight: `${minHeight || 'initial'}`,
        minWidth: `${minWidth}`,
      }}
    >
      { chooseTypeInput(props, ErrorProps) }
      <ErrorMessage name={input.name} {...ErrorProps} />
    </div>
  );
};


InputForm.defaultProps = {
  meta: {},
  error: '',
  block: '',
  size: null,
  padding: 'initial',
  minWidth: 'initial',
  minHeight: 'initial',
  className: '',
};

InputForm.propTypes = {
  meta: PropTypes.oneOfType([
    PropTypes.object,
  ]),
  error: PropTypes.string,
  block: PropTypes.string,
  size: PropTypes.number,
  padding: PropTypes.string,
  minWidth: PropTypes.string,
  minHeight: PropTypes.string,
  className: PropTypes.string,
  input: PropTypes.oneOfType([
    PropTypes.object,
  ]).isRequired,
};


export default InputForm;
