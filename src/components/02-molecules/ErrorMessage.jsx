import React from 'react';
import styled from 'styled-components';
import shortid from 'shortid';
import { colors } from '../../utils/css/variables';


const ErrorMessage = ({
  name, touched, error, isSubmit,
}) => {
  let arrErrors = [];
  if ((touched || isSubmit)) {
    if (error) {
      arrErrors = [error] || [];
    }
  }
  const ErrorSpan = styled.span`
    color: ${colors.error_color};
    display: block;
    font-size: 0.85em;
    font-weight: normal;
    text-align:left;
    position: ${name === 'query' ? 'absolute' : 'initial'};
  `;
  return (
    arrErrors.map(el => (
      <ErrorSpan key={shortid.generate()} >{el}</ErrorSpan>
    )));
};

export default ErrorMessage;
