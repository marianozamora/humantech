import React from 'react';
import PropTypes from 'prop-types';
import Button from '../01-atoms/Button/Button';

const GroupButton = ({
  btnClass,
  btnLabel, isDisabled, reset, clearForm,
  customClass, btnContainerClass, btnColor, type, callBack, icon, withReset,
}) => {
  const ButtonProps = {
    type,
    color: btnColor,
    disabled: isDisabled,
  };
  if (type !== 'submit') {
    ButtonProps.onClick = () => callBack();
  }
  return (
    <div className={`form-group ${customClass}`}>
      <div className={`col-sm-3 ${btnContainerClass || 'pull-right'} u-align-flex`} style={{ display: 'flex' }}>
        {
          withReset && (
            <Button
              style={{ position: 'relative' }}
              onClick={() => { clearForm(); reset(); }}
              type="button"
              className="btn btn-default u-mr-10"
            >
                Limpiar
            </Button>
          )
        }


        <Button
          {...ButtonProps}
          style={{ position: 'relative', display: 'flex', justifyContent: 'center' }}
          className={`btn btn-primary ${btnClass}`}
        >
          {btnLabel}
        </Button>
      </div>
    </div>
  );
};

GroupButton.defaultProps = {
  redirectTo: '',
  btnClass: '',
  isDisabled: false,
  reset: () => {},
  clearForm: () => {},
  customClass: '',
  btnContainerClass: '',
  btnColor: '',
  type: '',
  icon: '',
  withReset: false,
  callBack: () => {},
};

GroupButton.propTypes = {
  redirectTo: PropTypes.string,
  btnClass: PropTypes.string,
  btnLabel: PropTypes.string.isRequired,
  isDisabled: PropTypes.bool,
  reset: PropTypes.func,
  clearForm: PropTypes.func,
  customClass: PropTypes.string,
  btnContainerClass: PropTypes.string,
  btnColor: PropTypes.string,
  type: PropTypes.string,
  callBack: PropTypes.func,
  icon: PropTypes.string,
  withReset: PropTypes.bool,
};

export default GroupButton;
