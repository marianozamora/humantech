import React from 'react';
import { Field } from 'redux-form';
import shortid from 'shortid';
import PropTypes from 'prop-types';
import Icon from '../01-atoms/Icon/Icon';
import { colors } from '../../utils/css/variables';

const SelectForm = ({
  customName, options,
  customClass, customStyle,
  normalizeFn, formatFn, parseFn, label,
}) => (
  <div className={`${customStyle}`}>
    <label>{label}</label>
    <Field
      parse={(parseFn)}
      normalize={normalizeFn}
      component="select"
      name={customName}
      format={formatFn}
      className={`form-control ${customClass}`}
    >
      <option disabled value="" selected hidden>Selecciona una opcion</option>
      {options && options.map((obj, index) => (
        <option value={obj.value} key={shortid.generate()}>
          {' '}
          {obj.name}{' '}
        </option>))}
    </Field>
    <Icon type="arrow" size="1.5em" className="e-icon-arrow" fill={colors.yellow} />

  </div>);

SelectForm.defaultProps = {
  customName: '',
  customClass: '',
  customStyle: '',
  normalizeFn: data => data,
  formatFn: data => data,
  parseFn: data => data,
  label: '',
};

SelectForm.propTypes = {
  customName: PropTypes.string,
  options: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.bool,
  ]).isRequired,
  customClass: PropTypes.string,
  customStyle: PropTypes.string,
  normalizeFn: PropTypes.func,
  formatFn: PropTypes.func,
  parseFn: PropTypes.func,
  label: PropTypes.string,
};


export default SelectForm;
