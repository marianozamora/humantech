import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { colors, extra } from '../../utils/css/variables';
import media from '../../utils/css/media';


const Wrapper = ({ children, className }) => {
  const Section = styled.div`
    max-width: 1140px;
    display: inline-block;
    width:100%
    padding: 1em;
  `;

  return (
    <Section className={className}>
      { children }
    </Section>
  );
};

export const WrapperAbsolute = ({ children, className }) => {
  const Section = styled.div`
    background-color: ${colors.white};
    padding: 2.5em;
    border-radius: 10px;
    border: 1px solid ${colors.green};
    text-align: center;
    box-shadow: ${extra.shadow};
    max-width:410px;
    position:relative;
    &:after{
      content: '';
      position: absolute;
      z-index: -1;
      box-shadow: ${extra.box_shadow};
      width: 100%;
      left: 0;
      height: 100px;
      bottom: 0;
    }
    ${media.tablet_cutoff`
      padding: 1.5em;
    `}
  `;

  return (
    <Section className={className}>
      { children }
    </Section>
  );
};

export const WrapperResponsive = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100vh;
  width: 100vw;
`;
export default Wrapper;

Wrapper.defaultProps = {
  className: '',
};

Wrapper.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
};

WrapperAbsolute.defaultProps = {
  className: '',
};

WrapperAbsolute.propTypes = {
  /** Another Props. */
  /** Childrens inside wrapper . */
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
};
