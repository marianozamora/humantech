
import React, { Component } from 'react';
import ReactTable from 'react-table';
import PropTypes from 'prop-types';
import ReactPaginate from 'react-paginate';
import 'react-table/react-table.css';
import '../00-helpers/css/table.css';
import NotOrderMessage from '../03-components/MessageList/NotOrderMessage';

class DataTable extends Component {
  constructor(props) {
    super(props);
    this.state = { sorted: [], selected: 1 };
    this.onPageChange = this.onPageChange.bind(this);
  }

  onPageChange(index) {
    // WWWWWWSthis.props.method({ page: parseInt(index, 10) + 1 });
  }

  render() {
    const client = this.props;
    const clientData = this.props.data;
    const row = client.row === 0 ? 5 : client.row;

    const configReactTable = {
      resizable: false,
      minRows: row,
      page: client.page,
      showPagination: (client.showPagination),
    };

    if (typeof client.columns === 'function') {
      configReactTable.columns = client.columns(client.context);
    } else {
      configReactTable.columns = client.columns;
    }
    if ((clientData.data || clientData.results) &&
      Object.keys((clientData.data || clientData.results)).length !== 0) {
      configReactTable.data = (clientData.data) ? clientData.data.results : clientData.results;
    } else if (clientData.length) {
      configReactTable.data = clientData;
    }
    if ((clientData.data) && (clientData.data.num_pages)) {
      configReactTable.pages = clientData.data.num_pages;
    } else if (clientData.num_pages) {
      configReactTable.pages = clientData.num_pages;
    }

    return (
      <div>
        <ReactTable
          manual
          {...configReactTable}
          onPageChange={index => this.onPageChange(index)}
          sorted={this.state.sorted}
          noDataText={
            <NotOrderMessage
              status={client.statusOrder}
              link={client.link}
              text={client.text}
            />}
          getTrProps={this.props.linkTr}
          PaginationComponent={
            data => (
              <div className="e-pagination-container">
                {
                  (this.props.page !== 1) && (
                    <a onClick={() => this.props.method({ _page: 1 })}> {'<<'} </a>
                  )
                }
                <ReactPaginate
                  previousLabel="<"
                  nextLabel=">"
                  breakClassName="break-me"
                  pageCount={2}
                  disableInitialCallback
                  initialPage={this.props.page - 1}
                  breakLabel={<a href="">...</a>}
                  marginPagesDisplayed={2}
                  pageRangeDisplayed={3}
                  onPageChange={(page) => {
                    this.setState({ selected: page.selected + 1 });
                    this.props.method({ _page: page.selected + 1 });
                  }}
                  containerClassName="e-pagination"
                  subContainerClassName="pages pagination"
                  activeClassName="active"
                />
                {
                  (this.props.page !== 2) && (
                    <a onClick={() => this.props.method({ _page: 2 })} > {'>>'} </a>
                  )
                }
              </div>
            )}
        />
      </div>
    );
  }
}
/*
DataTable.defaultProps = {
  data: [],
}; */
DataTable.propTypes = {
  method: PropTypes.func.isRequired,
  data: PropTypes.oneOfType([
    PropTypes.array,
  ]).isRequired,
};
export default DataTable;
