import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import media from '../../utils/css/media';

const BackgroundPrivate = (props) => {
  const WrapBackground = styled.div`
    background-color: white;
    min-height: 100vh;
    padding-top:2em;
    display: flex;
    width: 100%;
    max-width: 1140px;
    margin: auto;
    ${media.desktop`
        padding:3em 2em 2em 2em
    `}
    `;

  return (
    <WrapBackground>
      {props.children}
    </WrapBackground>
  );
};


BackgroundPrivate.propTypes = {
  children: PropTypes.element.isRequired,
};

export default BackgroundPrivate;
