import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
/* import Logo from '../01-atoms/Logo/Logo';
 */
const Menu = (props) => {
  const WrapMenu = styled.div`
    display: none;
    width: 100%;
    z-index: 1;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    opacity: 0.95;
    position: fixed;
    background-color: #000;
  `;

  return (
    <WrapMenu className={props.className}>
      {props.children}
    </WrapMenu>
  );
};

Menu.defaultProps = {
  className: '',
};

Menu.propTypes = {
  className: PropTypes.string,
  children: PropTypes.element.isRequired,
};

export default Menu;
