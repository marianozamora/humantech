import React from 'react';
import PropTypes from 'prop-types';
import { H3 } from '../01-atoms/Title/Title';
import Wrapper from '../04-layouts/Wrapper';

/**
 *
 * @param {*} Header
 */

const Header = props =>
  (
    <Wrapper name={props.name} className={props.className}>
      {props.title && (
        <H3 style={
          {
            flexGrow: 1,
            textAlign: 'left',
            margin: '0',
            marginLeft: '1em',
            fontWeight: '900',
          }}
        >{props.title}
        </H3>
      )}
      { props.children }
    </Wrapper>
  );

Header.defaultProps = {
  name: '',
  className: '',
  isLogoBlack: false,
  logoClass: '',
  title: '',
};

Header.propTypes = {
  children: PropTypes.node.isRequired,
  name: PropTypes.string,
  className: PropTypes.string,
  isLogoBlack: PropTypes.bool,
  logoClass: PropTypes.string,
  title: PropTypes.string,
};

export default Header;
