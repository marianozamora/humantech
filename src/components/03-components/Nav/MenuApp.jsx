import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
/* import { Link } from './Nav.styles';
 */import WrapMenu from './WrapMenu';

class MenuApp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      height: '',
      name: localStorage.getItem('name')
    };
    this.updateDimensions = this.updateDimensions.bind(this);
  }

  componentDidMount() {
    this.updateDimensions();
    window.addEventListener('resize', this.updateDimensions);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateDimensions);
  }

  updateDimensions() {
    const el = document.querySelector('.u-container-header');
    this.setState({
      height: el.offsetHeight + 10,
    });
  }

  render() {
    const height = `${this.state.height}`;

    return [
      <div style={{marginRight:'2em'}}>{this.state.name}</div>,
      <WrapMenu height={height}>
        <nav>
          <ul>
            <li>
              <button
                style={{
                  color: '#00964B', background: 'transparent', border: 'none', cursor: 'pointer',
                }}
                onClick={() => this.props.fnLogout()}
              >Cerrar Sesión
              </button>
            </li>
          </ul>
        </nav>
      </WrapMenu>
    ];
  }
}


MenuApp.propTypes = {
  fnLogout: PropTypes.func.isRequired,
};

const mapStateToProps = state => (state.account);

const mapDispatchToProps = dispatch => (
  bindActionCreators({
  }, dispatch)
);
export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps,
)(MenuApp));
