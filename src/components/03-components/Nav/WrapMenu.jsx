import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { colors } from '../../../utils/css/variables';

const WrapMenu = (props) => {
  const Menu = styled.div`
    top:${props.height}px;
    right: -80px;
    z-index: 1;
    padding: 2em;
    display: none;
    text-align: left;
    position: absolute;
    color: ${colors.gray};
    background-color: #FFF;
    border-bottom-left-radius: 10px;
    border-bottom-right-radius: 10px;
    box-shadow: 1px 2px 3px ${colors.light_gray};
    &:before {
      top: -15px;
      left: 4em;
      content: '';
      position: absolute;
      border-bottom: 15px solid #FFF;
      border-left: 15px solid transparent;
      border-right: 15px solid transparent;
    }
  `;
  return (
    <Menu className="menu-app">
      {props.children}
    </Menu>
  );
};

WrapMenu.defaultProps = {
  height: 'initial',
};

WrapMenu.propTypes = {
  height: PropTypes.string,
  children: PropTypes.element.isRequired,
};

export default WrapMenu;
