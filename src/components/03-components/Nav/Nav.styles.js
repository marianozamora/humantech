import styled from 'styled-components';
import { NavLink } from 'react-router-dom';
import { colors } from '../../../utils/css/variables';

export const WrapNav = styled.div`
  width: 100%;
  padding: 6em 0;
  &>a {
    display: block;
    margin: 1em auto;
  }
  a:last-child {
    border: 2px solid #FFF;
    border-radius: 20px;
    color: #FFF;
    padding: 0.5em;
    cursor: pointer;
    width: 40%;
  }
`;

export const Link = styled(NavLink)`
  color: ${colors.gray};
`;
