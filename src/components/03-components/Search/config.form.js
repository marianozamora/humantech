
const searchFicForm = [
  {
    label: 'Seleccione una opcion', type: 'select', name: 'client_type', customName: 'client_type.id', selectValue: 'name', customClass: 'select-search', minheight: '50px', minWidth: 'inherit', customStyle: 'e-container-select',
  },
  {
    placeholder: 'Ingrese lo que desee buscar.', label: '', type: 'text', name: 'query', required: true, minWidth: 'inherit', minHeight: '50px', className: 'e-input-search',
  },
];

export default searchFicForm;
