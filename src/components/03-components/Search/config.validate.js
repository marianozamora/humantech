export const isNumber = value => !/^[-0-9+;]+$/.test(value);


export const validateMovie = (values) => {
    const errors = {};
  if (!values.name) {
    errors.name = 'Requerido';
  }
  if (!values.date) {
    errors.date = 'Requerido';
  }
  if (!Boolean(values.isActive)) {
    errors.isActive = 'Requerido';
  }

  return errors;
  
}

export const validateTurn = (values) => {
  const errors = {};
  if (!values.name) {
    errors.name = 'Requerido';
  }
  if (!values.turn) {
    errors.date = 'Requerido';
  }
  if (!Boolean(values.isActive)) {
    errors.isActive = 'Requerido';
  }

  return errors;

}