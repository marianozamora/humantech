import React from 'react';
import PropTypes from 'prop-types';
import { H1 } from '../../01-atoms/Title/Title';
import { colors } from '../../../utils/css/variables';
import Button from '../../01-atoms/Button/Button';
import styled from 'styled-components';
import Modal from 'react-modal';
import Form from '../Form';
import GroupButton from '../../02-molecules/GroupButton';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes } from "@fortawesome/free-solid-svg-icons";
import { validateMovie } from "./config.validate.js"


import {
  bindActionCreators
} from 'redux';
import {
  reduxForm,
  SubmissionError
} from 'redux-form';
import {
  connect
} from 'react-redux';


const Wrapper = styled.div`
    display: flex;
    align-items: center;
    text-align: center;
    justify-content: space-between;`;

const formStructure = [
  {
    label: 'Nombre:', type: 'text', name: 'name', size: 12,
    required: true
  },
  {
    label: 'Fecha de Publicacion:', type: 'date', name: 'date', size: 12,
  },
  {
    label: 'Estado',
    type: 'select',
    name: 'isActive', 
    customName: 'name',
    selectValue: 'isActive.id',
    customClass: 'select-search', minheight: '50px', minWidth: 'inherit',
    customStyle: 'e-container-select'
  },
];



const MovieData = function (props) {
  const {
    createMethod,
    editMethod,
    updateModal
  } = props;
  let subtitle;
  let [modalIsOpen, setIsOpen] = React.useState(false);
  const customStyles = {
    content: {
      top: '50%',
      left: '50%',
      right: 'auto',
      bottom: 'auto',
      width: '50%',
      marginRight: '-50%',
      transform: 'translate(-50%, -50%)'
    }
  };
  function openModalfn() {
    setIsOpen(true);
    updateModal(true);
  }


  function closeModal() {
    setIsOpen(false);
    updateModal(false);
  }

  const {
    handleSubmit,
    initialValues,
    pristine,
    submitting,
    valid
  } = props;

  const options = [{
      id: 1,
      name: 'Activo',
      value: true,
    },
    {
      id: 2,
      name: 'Inactivo',
      value: false,
    }
  ]
  return (
  <Wrapper>
    <H1 style={
      {
        color: colors.green_search_title,
        margin: '0',
        fontWeight: '900',
        fontSize: '30px',
      }}
    >
      PELICULAS.
    </H1>
    <Button 
      style={{width:"30%", margin: "initial"}} 
      type="default" 
      onClick = {
        () => openModalfn()
      } > 
      Nueva Pelicula</Button> 

    <Modal
      isOpen={props.movie.openModal}
      onRequestClose={closeModal}
      style={customStyles}
      contentLabel="Example Modal"
    >

      <h2 style={{color:'black'}}>{initialValues.id || initialValues.id === 0 ? 'Editar' : 'Nueva' } Pelicula</h2>
      <button className="closeModal" onClick={closeModal}>        
        <FontAwesomeIcon icon={faTimes} />
      </button>
        <div>
        <Form
          classNameForm={`u-mt20 e-search-component`}
          formStructure={formStructure}
          submitForm = {
            (data) => {
              data.isActive = (data.isActive === 'true' || (data.isActive) && data.isActive !== 'false') ? true : false;
              initialValues.id || initialValues.id === 0 ? editMethod(data.id, data) : createMethod(data);
              closeModal();
            }
            }
          align="column"
          options={options}
          handleSubmit={handleSubmit}
        >
          <GroupButton
            btnColor="dark_gray"
            type="submit"
            customClass="u-width-100"
            icon="search"
            isDisabled={pristine || submitting || !valid}
            btnContainerClass="u-align-center"
            btnLabel={'Enviar'}
          />
        </Form>
      </div>
    </Modal>
   </Wrapper>);


} 
MovieData.defaultProps = {
  name: '',
};

MovieData.propTypes = {
  name: PropTypes.string,
};

const mapStateToProps = state => {
  return ({
    movie: state.movie,
    initialValues: {
     ...state.movie.movieSelected,

    }
  });
}



const mapDispatchToProps = dispatch => (
  bindActionCreators({
  }, dispatch)
);


const MoviesTitle = reduxForm(
{
  form: 'formMovies',
  enableReinitialize: true,
  validate: validateMovie
})(MovieData);


export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MoviesTitle);
