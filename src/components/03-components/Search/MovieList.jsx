import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import {
  getMovies,
  deleteMovies,
  getMovie as editMovie,
} from '../../../actions/movieActions';
import DataTable from '../../04-layouts/DataTable';
import {
  statusFormat,
  dateFormat,
  actionFormat,
} from '../../../utils/js/formatsTable';

export const ClientTableColumn = (deleteMoviesFn, getMovieById) => [
  {
    Header: 'ID',
    accessor: 'id',
  },
  {
    Header: 'Nombre de la Pelicula',
    accessor: 'name',
  },
  {
    Header: 'Fecha De Publicacion',
    accessor: 'date',
    Cell: row => dateFormat(row.original.date),
  },
  {
    Header: 'Estado',
    accessor: 'isActive',
    Cell: row => statusFormat(row.original.isActive),
    width: 110,

  },
  {
    Header: 'Acciones',
    accessor: 'id',
    Cell: row => actionFormat(row.original.id, deleteMoviesFn, getMovieById),

  },
];
      

class ActiveTab extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.getMovies();
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.needRefresh !== this.props.needRefresh && nextProps.needRefresh) {
      this.props.getMovies(nextProps.filters);
    }
  }

  render() {
    const {
      data, context,
    } = this.props;
    
    const StyledOrderTable = styled.div`
        min-height: 200px;
        padding:.5em;
    `;
    return (
      <StyledOrderTable>
        {
          (data) && (
            <DataTable
              data={data}
              columns = {
                () => ClientTableColumn(this.props.deleteMovies, this.props.editMovie)
              }
              context={context}
              statusOrder="Activos"
              showPagination={false}
              row={(data.results) ? data.results.length : 0}
              detail={false}
            />
          )
        }
      </StyledOrderTable>
    );
  }
}

ActiveTab.defaultProps = {
  context: this,
};

ActiveTab.propTypes = {
  context: PropTypes.oneOfType([
    PropTypes.object,
  ]),
  data: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.bool,
  ]).isRequired,
};


const mapStateToProps = state => (state.movie);

const mapDispatchToProps = dispatch => (
  bindActionCreators({
    getMovies,
    deleteMovies,
    editMovie
  }, dispatch)
);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withRouter(ActiveTab));
