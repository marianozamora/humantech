import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import {
  getTurns,
  deleteTurns,
  editTurn,
  getTurnById
} from '../../../actions/movieActions';
import DataTable from '../../04-layouts/DataTable';
import {
  statusFormat,
  actionFormatTurn,
} from '../../../utils/js/formatsTable';

export const TurnTableColumn = (deleteMoviesFn, getTurnByIdFn, movieId) => [
  {
    Header: 'ID',
    accessor: 'id',
  },
  {
    Header: 'Turno',
    accessor: 'turn',
    Cell: row => row.original.turn,
    },
  {
    Header: 'Estado',
    accessor: 'isActive',
    Cell: row => statusFormat(row.original.isActive),
    width: 110,

  },
  {
    Header: 'Acciones',
    accessor: 'id',
    Cell: row => actionFormatTurn(row.original.id, deleteMoviesFn, getTurnByIdFn, movieId),

  },
];
      

class TurnsList extends Component {
  componentDidMount() {
    this.props.getTurns(this.props.routeProps.match.params.id);
  }
  componentWillReceiveProps(nextProps) {
    if ((nextProps.needRefreshturn !== this.props.needRefreshturn) ) {
      this.props.getTurns(this.props.routeProps.match.params.id);
    }
  }

  render() {
    const {
      turnsData, context,
    } = this.props;
    
    const StyledOrderTable = styled.div`
        min-height: 200px;
        padding:.5em;
    `;
    return (
      <StyledOrderTable>
        {
          (turnsData) && (
            <DataTable
              data={turnsData}
              columns = {
                () => TurnTableColumn(this.props.deleteTurns, this.props.getTurnById, this.props.routeProps.match.params.id)
              }
              context={context}
              showPagination={false}
              row={(turnsData.results) ? turnsData.results.length : 0}
              detail={false}
            />
          )
        }
      </StyledOrderTable>
    );
  }
}

TurnsList.defaultProps = {
  context: this,
  method: () => {},
};

TurnsList.propTypes = {
  filters: PropTypes.oneOfType([
    PropTypes.object,
  ]).isRequired,
  context: PropTypes.oneOfType([
    PropTypes.object,
  ]),
  method: PropTypes.func,
  data: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.bool,
  ]).isRequired,
};


const mapStateToProps = state => (state.movie);

const mapDispatchToProps = dispatch => (
  bindActionCreators({
    getTurns,
    deleteTurns,
    editTurn,
    getTurnById
  }, dispatch)
);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withRouter(TurnsList));
