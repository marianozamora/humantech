import React from 'react';
import PropTypes from 'prop-types';
import { ContainerMessage } from './MessageList.styles';
import HomeLink from '../../01-atoms/HomeLink/HomeLink';

const NotOrderMessage = ({ status, link, text }) => (
  <ContainerMessage className="gray-background">
    <p>No se encontraron {status}</p>
    {
      link && (
        <p className="u-mt30"><HomeLink to={link}>{text}</HomeLink></p>
      )
    }
  </ContainerMessage>
);

NotOrderMessage.defaultProps = {
  link: '',
  text: '',
};

NotOrderMessage.propTypes = {
  status: PropTypes.string.isRequired,
  link: PropTypes.string,
  text: PropTypes.string,
};

export default NotOrderMessage;
