import styled from 'styled-components';

export const Container = styled.div`
  padding: 4em;
`;

export const ContainerMessage = styled.div`
  padding: 0.5em 0 1em;
`;
