import styled from 'styled-components';
import media from '../../../utils/css/media';
import { colors } from '../../../utils/css/variables';
import HomeLink from '../../01-atoms/HomeLink/HomeLink';
import { NavItem } from '../../01-atoms/Link/Link';

export const WrapNavApp = styled.div`
  color: #FFF;
  display: flex;
  cursor: pointer;
  align-items: center;
  flex-wrap: nowrap;
  justify-content: space-around;
`;

export const WrapElements = styled.nav`
  text-align: left;
  display: flex;
  align-items:center;
  flex-grow: 1;
  justify-content: space-around;
`;

export const LinkOrder = HomeLink.extend`
  margin: 0 1em;
  padding: 0.5em 1em;
  ${media.desktop`
    padding: 0.5em;
  `}
`;

export const WrapElement = styled.span`
  margin: 0 2em;
  ${media.desktop`
    margin: 0;
  `}
`;

export const Item = NavItem.extend`
  margin: 0 1em;
  font-size: 1em;
  color:white;
`;

export const WrapPurse = styled.span`
  background: ${colors.purse_gray};
  border-radius: 10px;
  padding: 1px 5px;
  margin-left: 5px;
  display: flex;
  align-items: flex-start;
  flex-direction:column;
  ${media.desktop`
    display: none;
  `}
`;

export const Img = styled.img`
  width: 20px;
  ${media.desktop`
    display: none;
  `}
`;

export const WrapInfo = styled.div`
  cursor: pointer;
  display: flex;
  align-items: center;
`;

export const WrapElementInfo = styled.div`
  display: inline-block;
`;

export const Image = styled.img`
  border-radius: 50%;
  height: 55px;
  width: 55px;
`;

export const Name = styled.p`
  margin-right: 1em;
  ${media.desktop`
    margin-right: 0;
  `}
`;

export const HelpCenter = styled.span`
-ms-flex-item-align: center;
-ms-grid-row-align: center;
align-self: center;
max-width: 0;
opacity: 0;
white-space: nowrap;
font-size:0.7em;
transition: all 1s ease-out;`;

export const LinkHelp = styled.a`
display : -webkit-box;
display : -ms-flexbox;
display : flex;
position : relative;
overflow : hidden;
// padding-left : 8 px;
// padding-right : 8 px;
&:hover > span {
  opacity:1;
  max-width:100px;
}
`;

export const IconHelp = styled.i`
// &:hover{
//   color:blue
// }
`;
