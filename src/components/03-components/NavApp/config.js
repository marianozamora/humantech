export const NavBusiness = [
  { label: 'Activo', value: '/activo' },
  { label: 'Historial', value: '/historial' },
  { label: 'Borrador', value: '/borradores' },
  { label: 'Mi Cuenta', value: '/mi-cuenta/perfil' },
  { label: 'Mis Facturas', value: '/facturas' }];

export const NavPerson = [
  { label: 'Activo', value: '/activo' },
  { label: 'Historial', value: '/historial' },
  { label: 'Borrador', value: '/borradores' },
  { label: 'Mi Cuenta', value: '/mi-cuenta/perfil' }];
