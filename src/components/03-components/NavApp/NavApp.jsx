import React from 'react';
import PersonalInfo from './PersonalInfo';
import { WrapNavApp } from './NavApp.styles';

const NavApp = () => (
  <WrapNavApp>
    <PersonalInfo />
  </WrapNavApp>
);

export default NavApp;
