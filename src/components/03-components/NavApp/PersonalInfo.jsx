import React, { Component } from 'react';
import { WrapInfo, WrapElementInfo } from './NavApp.styles';
import Icon from '../../01-atoms/Icon/Icon';

export default class PersonalInfo extends Component {
  constructor(props) {
    super(props);
    this.state = { active: false };
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    const menu = document.querySelector('.menu-app');

    if (menu.style.display === 'block') menu.style.display = 'none';
    else menu.style.display = 'block';

    if (!this.state.active) this.setState({ active: true });
    else this.setState({ active: false });
  }

  render() {
    const active = this.state.active ? 'active' : '';
    const classes = `${active}`;
    return (
      <div style={{ display: 'flex', alignItems: 'center' }}>
        <WrapInfo className={classes} onClick={this.handleClick}>
          <WrapElementInfo>
            <Icon type="user-circle" size="2em" className="u-mr-5" fill="#00964B" />
          </WrapElementInfo>
        </WrapInfo>
      </div>
    );
  }
}
