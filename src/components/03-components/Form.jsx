import React from 'react';
import { Field } from 'redux-form';
import PropTypes from 'prop-types';
import InputForm from '../02-molecules/InputForm';

const Form = (props) => {
  const {
    handleSubmit, submitForm, formStructure, children,
    classNameForm, align, widthForm, options, placeholderDinamic,
  } = props;
  return (
    <form onSubmit={handleSubmit(submitForm)} className={`form-horizontal ${classNameForm}`} noValidate autoComplete="off">
      <div style={{ width: widthForm || '100%' }} className={`col-lg-12  u-flex-center u-flex-${align} `} >
        {
          formStructure.map((obj, index) => (
            <Field
              key={index}
              {...obj}
              options={options}
              placeholderDinamic={placeholderDinamic}
              clasName="col-lg-6"
              component={InputForm}
            />
          ))
        }
      </div>
      {children}
    </form>
  );
};

Form.defaultProps = {
  classNameForm: '',
  placeholderDinamic: false,
  align: '',
  widthForm: '',
  options: false,
};

Form.propTypes = {
  classNameForm: PropTypes.string,
  submitForm: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  formStructure: PropTypes.oneOfType([
    PropTypes.array,
  ]).isRequired,
  placeholderDinamic: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.bool,
  ]),
  align: PropTypes.string,
  children: PropTypes.element.isRequired,
  widthForm: PropTypes.string,
  options: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.bool,
  ]),
};

export default Form;
