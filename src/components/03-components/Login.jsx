import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import shortid from 'shortid';
import { H4 } from '../01-atoms/Title/Title';
import Button from '../01-atoms/Button/Button';
import ErrorMessage from '../02-molecules/ErrorMessage';
import { loginUser } from '../../actions/userActions';
import { onChange, onSubmit } from '../../utils/js/events';
import InputForm from '../02-molecules/InputForm';


const formStructure = [
  {
    label: 'Email', type: 'email', name: 'email', required: true,
  },
  {
    label: 'Contraseña', type: 'password', name: 'password', required: true,
  },
];

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = { email: '', password: '' };
    this.onChange = onChange.bind(this);
    this.onSubmit = onSubmit.bind(this);
  }

  render() {
    const { errors } = this.props;
    return (
      <div>
        <form className="u-form-login-flex" onSubmit={e => this.onSubmit(e, this, 'loginUser', this.state)} noValidate>
          <H4>INICIAR SESIÓN</H4>
          { errors && errors.non_field_errors && (
            <ErrorMessage isSubmit error={errors.non_field_errors} />
          )}
          {
            formStructure.map(el => (
              <InputForm
                key={shortid.generate()}
                type={el.type}
                name={el.name}
                value={this.state[el.name]}
                onChange={e => onChange(e, this)}
                label={el.label}
                required={el.required}
                error={errors[el.name]}
              />
            ))
          }
          <Button color="green">Iniciar Sesión</Button>
        </form>
      </div>
    );
  }
}

/* Login.defaultProps = {
  errors: {},
};
 */
Login.propTypes = {
  errors: PropTypes.oneOfType([
    PropTypes.object,
  ]),
};


const mapStateToProps = state => (state.account.login);

const mapDispatchToProps = dispatch => (
  bindActionCreators({
    loginUser,
  }, dispatch)
);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withRouter(Login));
