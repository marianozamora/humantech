import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { reduxForm, SubmissionError } from 'redux-form';
import Form from '../../../03-components/Form';
import GroupButton from '../../../02-molecules/GroupButton';
import { generateFetch } from '../../../../utils/js/fetch';
import { login } from '../../../../actions/accountActions';
import validate from './config.validate';

const basepath = `${process.env.REACT_APP_BASE_DOMAIN}auth/login/`;

class Login extends Component {
  constructor(props) {
    super(props);
    this.submit = this.submit.bind(this);
  }
  componentWillReceiveProps(nextProps) {
    if ((nextProps.isAuthenticated !== this.props.isAuthenticated) && nextProps.isAuthenticated) this.props.history.push('/activo');
  }
  submit(values) {
    const url = `${basepath}`;
    const config = {
      method: 'POST',
      data: values,
      url,
    };
    const request = generateFetch(config);
    return request.then((el) => {
      this.props.login(el.data);
    }).catch((error) => {
      if (error.response) {
        throw new SubmissionError(error.response.data);
      }
    });
  }


  render() {
    const { formStructure, handleSubmit } = this.props;
    return (
      <Form align="column" formStructure={formStructure} submitForm={this.submit} handleSubmit={handleSubmit} >
        <GroupButton
          btnColor="green"
          type="submit"
          customClass="u-width-100"
          btnContainerClass="u-align-center"
          btnLabel="Ingresar"
        />
      </Form>
    );
  }
}


Login.propTypes = {
  formStructure: PropTypes.oneOfType([
    PropTypes.array,
  ]).isRequired,
  handleSubmit: PropTypes.func.isRequired,
  login: PropTypes.func.isRequired,
  isAuthenticated: PropTypes.bool.isRequired,
  history: PropTypes.oneOfType([
    PropTypes.object,
  ]).isRequired,
};


const mapStateToProps = state => (state.account.login);


const mapDispatchToProps = dispatch => (
  bindActionCreators({
    login,
  }, dispatch)
);

const ConnectLoginForm = connect(mapStateToProps, mapDispatchToProps)(Login);

const LoginForm = reduxForm({
  form: 'login',
  validate,

})(ConnectLoginForm);

export default LoginForm;
