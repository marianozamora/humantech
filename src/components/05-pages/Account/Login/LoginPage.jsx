import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { WrapperAbsolute, WrapperResponsive } from '../../../04-layouts/Wrapper';
import formStructure from './config.form';
import LoginForm from './LoginForm';

class LoginPage extends Component {
  
  
  componentWillMount () {
    if(localStorage.token_web)
      this.props.history.push('/search')
  }
  
  render(){
    return(
      <WrapperResponsive >
        <WrapperAbsolute className="e-container-login">
          <LoginForm formStructure={formStructure} {...this.props} />
        </WrapperAbsolute>
      </WrapperResponsive>
    )
  }
} 
LoginPage.defaultProps = {
  logoClass: '',
  isLogoBlack: false,
};

LoginPage.propTypes = {
  isLogoBlack: PropTypes.bool,
  logoClass: PropTypes.string,
};

export default LoginPage;
