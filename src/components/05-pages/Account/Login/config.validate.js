const validate = (values) => {
  const errors = {};
  if (!values.password) {
    errors.password = 'Requerido';
  }

  if (!values.username) {
    errors.username = 'Requerido';
  }

  return errors;
};

export default validate;
