const formStructure = [
  {
    label: 'Usuario:', type: 'text', name: 'username', size: 12,
  },
  {
    label: 'Contraseña:', type: 'password', name: 'password', size: 12,
  },
];

export default formStructure;
