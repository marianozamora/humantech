import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import SearchClientData from '../../03-components/Search/MovieData';
import SearchList from '../../03-components/Search/MovieList';
import {
  createMovies,
  updateModal,
  editMovie
} from '../../../actions/movieActions';

class SearchPage extends Component {
  render() {
    return (
      <div>
        <SearchClientData 
          updateModal = {
            this.props.updateModal
          }
          createMethod={this.props.createMovies} 
          editMethod = {
            this.props.editMovie
          }
          name={localStorage.getItem('name')} />
        <SearchList data={this.props.data} />
      </div>);
  }
}

SearchPage.propTypes = {
  search_types: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.bool,
  ]).isRequired,
  data: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.bool,
  ]).isRequired,
};

const mapDispatchToProps = dispatch => (
  bindActionCreators({
    createMovies,
    updateModal,
    editMovie
  }, dispatch)
);

const mapStateToProps = state => (state.searchClient);

export default connect(mapStateToProps, mapDispatchToProps)(SearchPage);
