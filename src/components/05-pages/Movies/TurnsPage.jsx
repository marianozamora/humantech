import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import TurnsData from '../../03-components/Search/TurnsData';
import TurnsList from '../../03-components/Search/TurnsList';
import {
  createTurn,
  updateModal,
  editTurn
} from '../../../actions/movieActions';

class TurnsPage extends Component {
  render() {
    return (
      <div>
        <TurnsData 
          updateModal = {
            this.props.updateModal
          }
          createMethod = {
            this.props.createTurn
          }
          editMethod = {
            this.props.editTurn
          }
          movieId = {
            this.props.routeProps.match.params.id
          }
          name={localStorage.getItem('name')} />
        <TurnsList data={this.props.data} routeProps={this.props.routeProps} /> 
      </div>);
  }
}

TurnsPage.propTypes = {
  data: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.bool,
  ]).isRequired,
};

const mapDispatchToProps = dispatch => (
  bindActionCreators({
    createTurn,
    updateModal,
    editTurn
  }, dispatch)
);

const mapStateToProps = state => (state.searchClient);

export default connect(mapStateToProps, mapDispatchToProps)(TurnsPage);
