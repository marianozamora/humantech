import { LOGIN_SUCCESS, LOGOUT_SUCCESS, GET_DATA_USER } from '../../actions/userActions';

export default function reducer(state = {
  isAuthenticated: !!localStorage.getItem('token_web'),
  isFetching: false,
  data: {},
  errors: {},
}, action) {
  switch (action.type) {
    case GET_DATA_USER:
      return Object.assign({}, state, {
        data: { name: action.data.name },
      });
    case LOGIN_SUCCESS:
      localStorage.setItem('token_web', action.data.access_token);
      localStorage.setItem('name', action.data.user);

      return Object.assign({}, state, {
        isFetching: false,
        isAuthenticated: true,
        data: { name: action.data },
      });

    case LOGOUT_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        isAuthenticated: false,
      });

    default:
      return state;
  }
}
