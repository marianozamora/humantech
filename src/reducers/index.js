import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

import loginReducer from './login/loginReducer';
import movieReducer from './movie/movieReducer';


export default combineReducers({
  form: formReducer,
  account: combineReducers({
    login: loginReducer,
  }),
  movie: movieReducer,
});
