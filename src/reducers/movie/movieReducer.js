import movieActions from '../../actions/movieActions';

export default function reducer(state = {
  isFetching: false,
  data: false,
  errors: {},
  needRefresh: false,
  movieSelected: false,
  needRefreshturn:false,
  openModal: false,
  turnsData: false,
  filters: {
    _page: 1,
  },
}, action) {
  switch (action.type) {
    case movieActions.MOVIES_GETDATA_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
        needRefresh: false,
      });
    case movieActions.MOVIES_GETDATA_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        errors: action.errors,
      });
    case movieActions.MOVIES_GETDATA_SUCCESS:
      return Object.assign({}, state, {
        data: action.data,
      });
    case movieActions.MOVIES_DELETE_SUCCESS:
    case movieActions.MOVIES_CREATE_SUCCESS:
      return Object.assign({}, state, {
        needRefresh: true,
      });
    case movieActions.MOVIES_FILTER_UPDATE:
      return Object.assign({}, state, {
        filters: {
          ...state.filters,
          ...action.data,
        },
        needRefresh: true,
      });
    case movieActions.CLOSE_MODAL:
      return Object.assign({}, state, {
        openModal: action.payload,
        movieSelected: false,
        turnSelected: false
      });
    case movieActions.MOVIES_EDIT_SUCCESS:
      return Object.assign({}, state, {
        needRefresh: true,
      });
    case movieActions.MOVIES_GETDATABYID_SUCCESS:
      return Object.assign({}, state, {
        movieSelected: {
          ...action.data,
        },
        openModal: true,
        needRefresh: false,
      });
    case movieActions.TURNS_GETDATA_SUCCESS:
      return Object.assign({}, state, {
        needRefreshturn: false,
        turnsData: action.data
      });
    case movieActions.TURNS_CREATE_SUCCESS:
      return Object.assign({}, state, {
        needRefreshturn: true,
      });
    case movieActions.TURNS_DELETE_SUCCESS:
    case movieActions.TURNS_EDIT_SUCCESS:
      return Object.assign({}, state, {
        needRefreshturn: true,
        turnSelected: false,
      });
    case movieActions.TURNS_GETDATABYID_SUCCESS:
      return Object.assign({}, state, {
        turnSelected: {
          ...action.data,
        },
        openModal: true,
        needRefreshturn: false,
      });
    default:
      return state;
  }
}
