import axios from 'axios';
import ip from 'ip';

const BASE_URL = process.env.REACT_APP_BASE_API_URL;
const BASE_DOMAIN = process.env.REACT_APP_BASE_DOMAIN;
const GET_IP = ip.address();
const BASE_LOCAL_URL = (GET_IP) ? `http://${GET_IP}:3006/` : 'http://localhost:3006/';

const calculateUrl = (isPublic, localAPI, endpoint) => {
  let url;
  if (localAPI) {
    url = `${BASE_LOCAL_URL}${endpoint}`;
  } else if (isPublic) {
    url = `${BASE_DOMAIN}${endpoint}`;
  } else {
    url = `${BASE_URL}${endpoint}`;
  }
  return url;
};

export function callApi(
  method, endpoint, data, authenticated,
  localAPI, isPublic,
) {
  const token = localStorage.getItem('token_web') || null;

  const config = {
    method,
    url: calculateUrl(isPublic, localAPI, endpoint),
    data,
  };
  if (authenticated) {
    if (token) { config.headers = { Authorization: `Bearer ${token}`, 'Accept-Language': 'es' }; } else { window.location = '/login/'; }
  }

  return axios(config);
}

export const CALL_API = Symbol('Call API');

export default () => next => (action) => {
  const callAPI = action[CALL_API];

  if (typeof callAPI === 'undefined') {
    return next(action);
  }

  const {
    method, endpoint, data, authenticated, types, extra,
    localAPI, isPublic, msg, storeAPI, register, setToken,
  } = callAPI;
  const [requestType, successType, errorType] = types;

  next({ type: requestType, extra });

  return callApi(
    method, endpoint, data, authenticated,
    localAPI, isPublic, msg, storeAPI, register, setToken,
  ).then(response => next({
    extra,
    data: response.data,
    type: successType,
  }))
    .catch((error) => {
      if (error.response && error.response.status === 401) {
        localStorage.clear();
        window.location = '/login';
      } else if (error.response.status === 500) {
        return next({
          errors: 'Parece que algo salió mal, por favor inténtelo de nuevo, o contáctese con soporte@company.pe',
          type: errorType,
        });
      }
      return next({
        errors: error.response
          ? error.response.data
          : { error: 'error' },
        type: errorType,
      });
    });
};
