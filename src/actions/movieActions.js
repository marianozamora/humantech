import { param } from 'jquery';
import { CALL_API } from '../middleware/api';

const searchActions = {};

searchActions.ACTIVE_FILTER_UPDATE = 'ACTIVE_FILTER_UPDATE';
searchActions.SET_VIEW = 'SET_VIEW';
searchActions.MODAL_ACTIVE = 'MODAL_ACTIVE';
searchActions.CLOSE_MODAL = 'CLOSE_MODAL';
searchActions.OPEN_MODAL = 'OPEN_MODAL';


const genTypeActions = (entity, action) => {
  const arr = ['REQUEST', 'SUCCESS', 'FAILURE'];
  const arrType = arr.map((el) => {
    const type = `${entity.toUpperCase()}_${action.toUpperCase()}_${el}`;
    searchActions[type] = type;
    return type;
  });
  return arrType;
};


export function getMovie(id) {
  let url = 'movies/' + id;

  return {
    [CALL_API]: {
      method: 'GET',
      endpoint: url,
      authenticated: true,
      localAPI: true,
      types: genTypeActions('movies', 'getDataByID'),
    },
  };
}

export function getTurns(movieId) {
  let url = `movies/${movieId}/turns`;

  return {
    [CALL_API]: {
      method: 'GET',
      endpoint: url,
      authenticated: true,
      localAPI: true,
      types: genTypeActions('turns', 'getData'),
    },
  };
}



export function getMovies(params = {}) {
  let url = 'movies?_page=1&_limit=100';
  const qs = param(params, true);
  if (qs) url += (`?${qs}`);

  return {
    [CALL_API]: {
      method: 'GET',
      endpoint: url,
      authenticated: true,
      localAPI: true,
      types: genTypeActions('movies', 'getData'),
    },
  };
}

export function deleteMovies(id) {
  let url = 'movies/' + id;
  return {
    [CALL_API]: {
      method: 'DELETE',
      endpoint: url,
      authenticated: true,
      localAPI: true,
      types: genTypeActions('movies', 'delete'),
    },
  };
}

export function createMovies(data) {
  let url = 'movies/';
  return {
    [CALL_API]: {
      method: 'POST',
      endpoint: url,
      authenticated: true,
      data,
      localAPI: true,
      types: genTypeActions('movies', 'create'),
    },
  };
}

export function editMovie(id, data) {
  let url = 'movies/'+ id;
  return {
    [CALL_API]: {
      method: 'PUT',
      endpoint: url,
      authenticated: true,
      data,
      localAPI: true,
      types: genTypeActions('movies', 'edit'),
    },
  };
}


export function updateModal(value) {
  return function (dispatch) {
    const action = {
      type: searchActions.CLOSE_MODAL,
      payload: value,
    };
    dispatch(action);
  };
}

export function createTurn(movieId, data) {
  let url = 'movies/' + movieId + '/turns';
  return {
    [CALL_API]: {
      method: 'POST',
      endpoint: url,
      authenticated: true,
      data,
      localAPI: true,
      types: genTypeActions('turns', 'create'),
    },
  };
}

export function deleteTurns(movieId, id) {
  let url = 'movies/' + movieId + '/turns/' + id;
  return {
    [CALL_API]: {
      method: 'DELETE',
      endpoint: url,
      authenticated: true,
      localAPI: true,
      types: genTypeActions('turns', 'delete'),
    },
  };
}

export function getTurnById(movieId, id) {
  let url = 'movies/' + movieId + '/turns/' + id;

  return {
    [CALL_API]: {
      method: 'GET',
      endpoint: url,
      authenticated: true,
      localAPI: true,
      types: genTypeActions('turns', 'getDataByID'),
    },
  };
}

export function editTurn(movieId, id, data) {
  let url = 'movies/' + movieId + '/turns/' + id;
  return {
    [CALL_API]: {
      method: 'PUT',
      endpoint: url,
      authenticated: true,
      data,
      localAPI: true,
      types: genTypeActions('turns', 'edit'),
    },
  };
}


export default searchActions;
