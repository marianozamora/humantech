/* import { CALL_API } from '../middleware/api';
 */
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const GET_DATA_USER = 'GET_DATA_USER';


export const login = data => dispatch => dispatch({ type: LOGIN_SUCCESS, data });

export const getUser = data => dispatch => dispatch({ type: GET_DATA_USER, data });
