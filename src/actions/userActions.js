import { CALL_API } from '../middleware/api';

export const LOGIN_REQUEST = 'LOGIN_REQUEST';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAILURE = 'LOGIN_FAILURE';
export const LOGOUT_REQUEST = 'LOGOUT_REQUEST';
export const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS';
export const CLIENT_TYPE_REQUEST = 'CLIENT_TYPES_REQUEST';
export const CLIENT_TYPE_SUCCESS = 'CLIENT_TYPES_SUCCESS';
export const CLIENT_TYPE_FAILURE = 'CLIENT_TYPES_FAILURE';
export const GET_DATA_USER = 'GET_DATA_USER';

export const getUser = data => dispatch => dispatch({ type: GET_DATA_USER, data });

export const logoutUser = () => (dispatch) => {
  dispatch({ type: LOGOUT_REQUEST });
  localStorage.clear();
  dispatch({ type: LOGOUT_SUCCESS });
};


export function loginUser(creds) {
  return {
    [CALL_API]: {
      method: 'POST',
      endpoint: 'authenticate/jwt/',
      authenticated: false,
      data: creds,
      types: [LOGIN_REQUEST, LOGIN_SUCCESS, LOGIN_FAILURE],
    },
  };
}

export function clientTypes() {
  return {
    [CALL_API]: {
      method: 'GET',
      endpoint: 'api/client_type/',
      authenticated: false,
      isPublic: true,
      types: [CLIENT_TYPE_REQUEST, CLIENT_TYPE_SUCCESS, CLIENT_TYPE_FAILURE],
    },
  };
}
