/* config-overrides.js */
const { WebpackWarPlugin } = require('webpack-war-plugin');

module.exports = function override(config, env) {
  if (!config.plugins) {
    config.plugins = [];
  }
  if (env !== 'development') { config.plugins.push(new WebpackWarPlugin()); }
  return config;
};
